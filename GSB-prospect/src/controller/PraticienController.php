<?php
/**
 * File :        PraticienController.php
 * Location :    gsb_prospects/src/controller/PraticienController.php
 * PHP Version : 7.0
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\controller;

use gsb_prospects\kernel\Route;
use gsb_prospects\kernel\Router;
use gsb_prospects\model\dao\PraticienDAO;
use gsb_prospects\model\objects\Praticien;
use gsb_prospects\view\View;

/**
 * Class PraticienController
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
final class PraticienController extends AbstractController implements IController
{
    /**
     * __construct
     */
    public function __construct()
    {
        $this->_dao = new PraticienDAO();
        $this->_router = new Router();
        // 2nd level route definition
        $this->_router->addRoute(new Route("/praticiens", "PraticienController", "listAction", "praticien_list"));
        $this->_router->addRoute(new Route("/praticien/create", "PraticienController", "createAction", "praticien_create"));
        $this->_router->addRoute(new Route("/praticien/update/{id}", "PraticienController", "updateAction", "praticien_update"));
        $this->_router->addRoute(new Route("/praticien/delete/{id}", "PraticienController", "deleteAction", "praticien_delete"));
    }

    /**
     * Procedure defaultAction
     *
     * @return void
     */
    public function defaultAction()
    {
        $route = $this->_router->findRoute();
        if ($route) {
            $route->execute();
        } else {
            print("<p> Page inconnue.</p>" . PHP_EOL);
        }
    }

    /**
     * Procedure listAction
     *
     * @return void
     */
    public function listAction()
    {
        $view = new View("Praticien_List");

        $view->bind("title", "Liste des Praticiens");
        $view->bind("objectName", "praticien");
        $view->bind("objectNamePlural", "praticiens");

        $basePath = $this->_router->getBasePath();
        $view->bind("basePath", $basePath);

        $objects = $this->_dao->findAll();
        $view->bind("objects", $objects);

        $view->display();
    }

    /**
     * Procedure createAction
     *
     * @return void
     */
    public function createAction()
    {
        if(!isset($_POST['ajout']))
        {
            // le formulaire n'est pas encore validé
            $view = new View("Praticien_Create");

            $view->bind("title", "Ajout d'un Praticien");
            $basePath = $this->_router->getBasePath();
            $view->bind("basePath", $basePath);
    
            $view->display();
        }
        else
        {
            
            // le formulaire est validé
            $nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING);           
            $prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_STRING);
            $adresse = filter_input(INPUT_POST, 'adresse', FILTER_SANITIZE_STRING);
            $id_Ville = filter_input(INPUT_POST, 'id_Ville', FILTER_SANITIZE_STRING);
            $id_Type_Praticien = filter_input(INPUT_POST, 'id_Type_Praticien', FILTER_SANITIZE_STRING);
            $dao = new PraticienDAO();
            $ajout = $dao->addPraticien($nom, $prenom, $adresse, $id_Ville, $id_Type_Praticien);
            
            $view = new View("Praticien_Create");
            $view->bind("title", "Ajout d'un Praticien");
            $basePath = $this->_router->getBasePath();
            $view->bind("basePath", $basePath);
            $view->display();

        }
    }

    public function updateAction()
    {
        $view = new View("Praticien_Update");

        $view->bind("title", "Modification d'un Praticien");

        $basePath = $this->_router->getBasePath();
        $view->bind("basePath", $basePath);

        $view->display();
    }

    public function deleteAction()
    {

        if(!isset($_GET["id"]))
        {
            // le formulaire n'est pas encore validé
            $view = new View("Praticien_Delete");

            $view->bind("title", "Suppression d'un Praticien");
            $basePath = $this->_router->getBasePath();
            $view->bind("basePath", $basePath);
    
            $view->display();
        }
        else
        {
            
        }
    }
}
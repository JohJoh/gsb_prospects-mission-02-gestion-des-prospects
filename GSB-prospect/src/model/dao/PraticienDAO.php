<?php
/**
 * File :        PraticienDAO.php
 * Location :    gsb_prospects/src/model/dao/PraticienDAO.php
 * PHP Version : 7.0
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\model\dao;

use \PDO;
use \PDOException;
use gsb_prospects\kernel\NotImplementedException;
use gsb_prospects\model\objects\Praticien;

/**
 * Class PraticienDAO
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
final class PraticienDAO extends AbstractDAO implements IDAO
{
    protected $table = "praticien";
    protected $class = "gsb_prospects\model\objects\Praticien";
    protected $fields = [ 
        "id", "nom", "prenom", "adresse" 
    ];

    public function addPraticien($nom, $prenom, $adresse, $id_Ville, $id_Type_Praticien)
    {
        // 1. Connexion
        $dbh = $this->getConnexion();

        // 2. Definition de la requête SQL
        $query = "
            INSERT INTO `praticien`
            (`nom`, `prenom`, `adresse`, `id_Ville`, `id_Type_Praticien`)
            VALUES
            (:nom, :prenom, :adresse, :id_Ville, :id_Type_Praticien);
        ";

        // 3. préparation de la requête
        $sth = $dbh->prepare($query);

        // 4. fourniture des paramètres
        $sth->bindParam(":nom", $nom, PDO::PARAM_STR);
        $sth->bindParam(":prenom", $prenom, PDO::PARAM_STR);
        $sth->bindParam(":adresse", $adresse, PDO::PARAM_STR);
        $sth->bindParam(":id_Ville", $id_Ville, PDO::PARAM_STR);
        $sth->bindParam(":id_Type_Praticien", $id_Type_Praticien, PDO::PARAM_STR);

        // 5. exécution de la requête préparée
        $res = $sth->execute();

        // 5.bis vérification de l'exécution
        if (!$res)
        {
           throw new Exception($sth->errorInfo()[2]);
        }
        else
        {
            // 6. récupération de l'id généré
            $id = $dbh->lastInsertId();
        }

        // 7. Déconnexion
        $this->closeConnexion();

        return $id;
    }


    public function deletePraticien($id)
    {
        // 1. Connexion
        $dbh = $this->getConnexion();

        // 2. Definition de la requête SQL
        $query = "
            DELETE FROM `praticien` WHERE id=:id LIMIT 1;"; // Je limite à une suppression en cas d'oublies ou problèmes

        // 3. préparation de la requête
        $sth = $dbh->prepare($query);

        // 4. fourniture des paramètres
        $sth->bindParam(":id", $id, PDO::PARAM_INT);

        // 5. exécution de la requête préparée
        $res = $sth->execute();

        // 5.bis vérification de l'exécution
        if (!$res)
        {
           throw new Exception($sth->errorInfo()[2]);
        }
        else
        {
            // 6. récupération de l'id généré
            $id = $dbh->lastInsertId();
        }

        // 7. Déconnexion
        $this->closeConnexion();

        return $id;
    }
}
